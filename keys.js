const keys = function (object) {
    if (typeof object === "object") {
        let key = [];
        for (let iteam in object) {
            key.push(iteam);
        }
        return key;
    }
    else {
        return [];
    }
}

module.exports = keys;