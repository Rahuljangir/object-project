const values = function (object) {
    if (typeof object === "object") {
        let value = [];
        for (let iteam in object) {
            value.push(object[iteam]);
        }
        return value;
    }
    else {
        return [];
    }
}
module.exports = values;