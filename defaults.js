const defaults = function (object, defaultProps) {
    if (typeof object === "object") {
        
        for (let iteam in defaultProps) {
            if(iteam in object){
                continue;
            }
            else{
                object[iteam] = defaultProps[iteam];
            }
        }
        return object;
    }
    else {
        return [];
    }
}

module.exports = defaults;