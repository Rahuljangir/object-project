const invert = function (object) {
    if (typeof object === "object") {
        let invertObject = {};
        for (let iteam in object) {
            invertObject[object[iteam]] = iteam;
        }
        return invertObject;
    }
    else {
        return [];
    }
}

module.exports = invert;