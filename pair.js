const pairs = function (object) {
    if (typeof object === "object") {
        let pair = [];
        for (let iteam in object) {
            pair.push([iteam, object[iteam]]);
        }
        return pair;
    }
    else {
        return [];
    }
}

module.exports = pairs;