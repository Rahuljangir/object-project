const mapObject = function (object, callBack) {
    if (typeof object === "object") {
        
        for (let iteam in object) {
            object[iteam] = callBack(object[iteam]);
        }
        return object;
    }
    else {
        return [];
    }
}

module.exports = mapObject;